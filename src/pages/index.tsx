import React from 'react'

import Layout from '../components/Layout'
import NeutrinoSimulator from '../components/NeutrinoSimulator'
import SEO from '../components/seo'

const IndexPage = () => (
  <Layout>
    <SEO title="Página inicial" />
    <NeutrinoSimulator />
  </Layout>
)

export default IndexPage

import React from 'react'

import Layout from '../components/Layout'
import SEO from '../components/seo'

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Não encontrada" />
    <h1>Página não encontrada!</h1>
    <p>Essa página que você está tentando acessar não existe!</p>
  </Layout>
)

export default NotFoundPage

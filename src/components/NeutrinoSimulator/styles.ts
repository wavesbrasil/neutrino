import styled from 'styled-components'

export const NeutrinoSimulatorWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  text-align: center;

  h1 {
    font-size: 3rem;
    font-family: 'PT Sans', sans-serif;
    font-weight: 400;
  }

  p {
    font-size: 1.6rem;
    font-family: 'PT Sans', sans-serif;
    margin-top: -1.5rem;
  }
`

export const SlidersWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;

  margin-top: 1rem;
`

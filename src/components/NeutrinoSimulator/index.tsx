import React, { useState } from 'react'

import Slider from 'components/Slider'
import Graphic from './Graphic'

import * as S from './styles'

const NeutrinoSimulator = () => {
  const [amountValue, setAmountValue] = useState('2000')
  const [yearValue, setYearValue] = useState('2')

  return (
    <S.NeutrinoSimulatorWrapper>
      <h1>Simulador de Rendimentos Neutrino</h1>
      <p>
        Compare os rendimentos Neutrino com CDB&apos;s e a Poupança. Simule agora mesmo e
        invista na melhor opção.
      </p>

      <S.SlidersWrapper>
        <Slider
          min={500}
          max={10000}
          value={amountValue}
          isCurrency
          step={500}
          label="Invista"
          onChange={value => setAmountValue(value)}
        />

        <Slider
          min={1}
          max={5}
          value={yearValue}
          isYear
          step={1}
          label="Período"
          onChange={value => setYearValue(value)}
        />
      </S.SlidersWrapper>

      <Graphic amount={Number(amountValue)} year={Number(yearValue)} />
    </S.NeutrinoSimulatorWrapper>
  )
}

export default NeutrinoSimulator

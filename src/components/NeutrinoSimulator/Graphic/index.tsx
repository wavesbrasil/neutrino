import React, { useEffect, useState } from 'react'

import { neutrinoAPI, savingsPercent } from 'services/api'

import BarCharts from 'components/BarCharts'

import * as S from './styles'

interface NeutrinoAPIResponse {
  'usdn-apy': {
    last: number
  }
  'brln-apy': {
    last: number
  }
}

interface Props {
  amount: number
  year: number
}

const colors = {
  savingArray: ['#F21818', '#B21212'],
  CDIArray: ['#FA5A00', '#CC500A'],
  USDNArray: ['#42a69d', '#2D8078'],
  BRLNArray: ['#2D942E', '#296629'],
}

const Graphic = ({ amount, year }: Props) => {
  const [percents, setPercents] = useState({
    saving: 0,
    CDI: 0.019,
    USDN: 0,
    BRLN: 0,
  })
  const [income, setIncome] = useState({
    saving: amount,
    CDI: amount,
    USDN: amount,
    BRLN: amount,
  })

  const [neutrinoAPIResponse, setNeutrinoAPIResponse] = useState<NeutrinoAPIResponse>({
    'usdn-apy': {
      last: 0,
    },
    'brln-apy': {
      last: 0,
    },
  })

  const NeutrinoAPI = async () => {
    const response = await neutrinoAPI.get('/neutrino/json')

    setNeutrinoAPIResponse(response.data)
  }

  useEffect(() => {
    if (neutrinoAPIResponse['usdn-apy'] || neutrinoAPIResponse['brln-apy']) {
      const lastUSDNPercent: number = neutrinoAPIResponse['usdn-apy'].last
      const lastBRLNPercent: number = neutrinoAPIResponse['brln-apy'].last

      handleModifyValue('USDN', lastUSDNPercent / 100)
      handleModifyValue('BRLN', lastBRLNPercent / 100)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [neutrinoAPIResponse])

  useEffect(() => {
    NeutrinoAPI()
    savingsPercent(handleModifyValue)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    handleModifyValue('saving', percents.saving)
    handleModifyValue('CDI', percents.CDI)
    handleModifyValue('USDN', percents.USDN)
    handleModifyValue('BRLN', percents.BRLN)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [amount, year])

  const handleModifyValue = (type: string, percent: number) => {
    let income = {}
    let newPercent = {}
    const incomeOperation = amount * percent * year + amount

    switch (type) {
      case 'saving':
        newPercent = {
          saving: percent,
        }
        income = {
          saving: incomeOperation,
        }
        break

      case 'USDN':
        newPercent = {
          USDN: percent,
        }
        income = {
          USDN: incomeOperation,
        }
        break

      case 'CDI':
        newPercent = {
          CDI: percent,
        }
        income = {
          CDI: incomeOperation,
        }
        break
      case 'BRLN':
        newPercent = {
          BRLN: percent,
        }
        income = {
          BRLN: incomeOperation,
        }
        break
    }

    setPercents(state => {
      return {
        ...state,
        ...newPercent,
      }
    })
    setIncome(state => {
      return {
        ...state,
        ...income,
      }
    })
  }

  return (
    <S.Container>
      <S.MainContainer>
        <BarCharts
          title="Poupança"
          titleColor="#c63030"
          color={colors.savingArray[0]}
          value={`R$ ${Number(income.saving).toFixed(0)}`}
          height={income.saving}
          colorArray={colors.savingArray}
        />

        <BarCharts
          title="CDB Banco Inter"
          titleColor="#FD7D09"
          color={colors.CDIArray[0]}
          value={`R$ ${Number(income.CDI).toFixed(0)}`}
          height={income.CDI}
          colorArray={colors.CDIArray}
        />

        <BarCharts
          title="USDN"
          titleColor="#05DA9B"
          color={colors.USDNArray[0]}
          value={`R$ ${Number(income.USDN).toFixed(0)}`}
          height={income.USDN}
          colorArray={colors.USDNArray}
        />

        <BarCharts
          title="BRLN"
          titleColor="#F5F600"
          color={colors.BRLNArray[0]}
          value={`R$ ${Number(income.BRLN).toFixed(0)}`}
          height={income.BRLN}
          colorArray={colors.BRLNArray}
        />
      </S.MainContainer>

      <S.BlackLine />
    </S.Container>
  )
}

export default Graphic

import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  margin: 0 auto;
  height: 25rem;
`

export const MainContainer = styled.div`
  display: flex;

  width: 100%;
  height: 100%;
`

export const BlackLine = styled.div`
  width: 100%;
  height: 0.5rem;
  background-color: grey;
`

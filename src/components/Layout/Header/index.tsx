import React from 'react'

import * as S from './styles'

const Header = () => {
  return (
    <S.HeaderWrapper>
      <S.SiteName>NeutrinoBrasil</S.SiteName>

      <S.Nav>
        <S.NavLink to="/">Home</S.NavLink>
        <S.NavLink to="/">Conheça</S.NavLink>
        <S.NavLink to="/">Invista</S.NavLink>
        <S.NavLink to="/">Aprenda</S.NavLink>
        <S.NavLink to="/">Sobre</S.NavLink>
        <S.NavLink to="/">Contato</S.NavLink>
      </S.Nav>
    </S.HeaderWrapper>
  )
}

export default Header

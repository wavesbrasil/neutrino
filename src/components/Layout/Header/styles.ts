import styled from 'styled-components'
import { lighten } from 'polished'

import { Link } from 'gatsby'

export const HeaderWrapper = styled.header`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 1rem 5rem;
  background: #000;
`

export const SiteName = styled.h1`
  color: #fff;
  font-family: 'Poppins', sans-serif;
`

export const Nav = styled.nav`
  display: flex;
  justify-content: space-around;
`

export const NavLink = styled(Link)`
  font-family: 'PT Sans', sans-serif;
  font-size: 1.7rem;
  margin: 0 1rem;
  color: #e4dede;
  text-decoration: none;
  transition: all 1s;

  &:hover {
    color: ${lighten(0.2, '#e4dede')};
  }
`

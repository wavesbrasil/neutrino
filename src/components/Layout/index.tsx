import React from 'react'

import GlobalStyles from 'styles/globalStyles'
import Header from './Header'

import * as S from './styles'

interface Props {
  children: React.ReactNode
}

const Layout = ({ children }: Props) => {
  return (
    <>
      <S.LayoutWrapper>
        <Header />

        <S.Main>{children}</S.Main>

        <S.Footer>
          <p>
            Feito por{' '}
            <a href="https://felipesuri.com" target="_blank" rel="noreferrer">
              felipesuri
            </a>
            , © {new Date().getFullYear()}, Todos os direitos reservados{' '}
            <a href="https://www.wavesbrasil.com.br" target="_blank" rel="noreferrer">
              WavesBrasil
            </a>
            .
          </p>
        </S.Footer>
      </S.LayoutWrapper>

      <GlobalStyles />
    </>
  )
}

export default Layout

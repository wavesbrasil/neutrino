import styled from 'styled-components'

export const LayoutWrapper = styled.div``

export const Main = styled.main`
  padding: 1rem 10rem;
`

export const Footer = styled.footer`
  margin-top: 1rem;
  display: flex;
  align-items: center;
  flex-direction: column;

  font-size: 1.2rem;
  color: #000;

  a {
    color: blue;
    text-decoration: none;

    &:visited {
      color: blue;
    }

    &:hover {
      text-decoration: underline;
    }
  }
`

import styled from 'styled-components'

export const SliderWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  margin: 2rem 1rem;
  padding: 1rem 3rem;

  > div {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    margin-top: -1rem;
  }
`

export const Title = styled.span`
  font-size: 1.4rem;
`

export const Value = styled.h3`
  font-size: 2rem;

  small {
    margin-right: 0.6rem;
  }
`

export const Input = styled.input`
  width: 100%;
  background: #d3d3d3;
  -webkit-appearance: none;
  -webkit-transition: 0.2s;
  height: 0.6rem;
  border-radius: 1rem;
  outline: none;
  opacity: 0.7;
  transition: opacity 0.2s;
  display: inline-block;

  &:hover {
    opacity: 1;
  }

  &::-webkit-slider-thumb {
    cursor: pointer;
  }

  &::-moz-range-thumb {
    background: #fff;
    cursor: pointer;
  }
`

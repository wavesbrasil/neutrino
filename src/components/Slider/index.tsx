import React, { ChangeEvent, useEffect, useState } from 'react'

import * as S from './styles'

interface Props {
  label: string
  value: string
  onChange(value: string): void
  min: number
  max: number
  step: number

  isCurrency?: boolean
  isYear?: boolean
}

const Slider = ({
  label,
  value,
  onChange,
  min,
  max,
  step,
  isCurrency,
  isYear,
}: Props) => {
  const [sliderValue, setSliderValue] = useState<string>('0')
  const [mouseState, setMouseState] = useState('')

  useEffect(() => {
    setSliderValue(value)
  }, [value, setSliderValue, isCurrency])

  const changeCallback = (event: ChangeEvent<HTMLInputElement>) => {
    setSliderValue(event.target.value)
  }

  useEffect(() => {
    if (mouseState === 'up') {
      onChange(sliderValue)
    }
  }, [mouseState, onChange, sliderValue])

  return (
    <S.SliderWrapper>
      <S.Title>{label}</S.Title>

      <div>
        {isCurrency && (
          <S.Value>
            <small>R$</small>
            {sliderValue}
          </S.Value>
        )}
        {isYear && <S.Value>{`${sliderValue} anos`}</S.Value>}
        {!isCurrency && !isYear && <S.Value>{sliderValue}</S.Value>}

        <S.Input
          type="range"
          value={sliderValue}
          min={min}
          max={max}
          step={step}
          onChange={changeCallback}
          onMouseDown={() => setMouseState('down')}
          onMouseUp={() => setMouseState('up')}
        />
      </div>
    </S.SliderWrapper>
  )
}

export default Slider

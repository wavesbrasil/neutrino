import styled, { css } from 'styled-components'
import { shade } from 'polished'

interface MakeBarProps {
  height: number
  colors: string[]
}

interface TextProps {
  color: string
}

export const Title = styled.p<TextProps>`
  margin-bottom: 0;
  color: ${props => props.color};
`

export const BarChartContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
`

export const Chart = css`
  margin-top: 1rem;
  width: 5.6rem;
  margin: 0 3rem;

  @media (max-width: 420px) {
    width: 3.4rem;
    margin: 0 0.6rem;
  }
`

export const MakeBar = styled.div<MakeBarProps>`
  height: ${props => props.height}px;
  width: 3rem;

  background-image: linear-gradient(
    to bottom,
    ${props => props.colors[0]},
    ${props => props.colors[1]}
  );

  &:hover {
    background: ${props => shade(0.3, props.colors[0])};
  }

  ${Chart};
`

export const Number = styled.span<TextProps>`
  font-size: 1.5rem;
  text-align: center;
  color: ${props => props.color};
`

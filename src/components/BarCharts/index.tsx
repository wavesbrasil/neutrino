import React from 'react'

import * as S from './styles'

interface Props {
  title: string
  titleColor: string
  color: string
  value: string | number
  height: number
  colorArray: string[]
}

const BarCharts = ({ title, titleColor, color, value, height, colorArray }: Props) => {
  return (
    <S.BarChartContainer>
      <S.Title color={titleColor}>{title}</S.Title>
      <S.Number color={color}>{value}</S.Number>
      <S.MakeBar height={height / 80} colors={colorArray} />
    </S.BarChartContainer>
  )
}

export default BarCharts

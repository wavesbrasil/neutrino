import axios from 'axios'
import XMLParser from 'react-xml-parser'

export const neutrinoAPI = axios.create({
  baseURL: 'https://dev.pywaves.org',
})

export const savingsPercent = async (setSavingPercent: Function): Promise<void> => {
  if (process.env.GATSBY_SAVING_API_URL) {
    const savingAPI = await fetch(process.env.GATSBY_SAVING_API_URL).then<string>(
      (results): Promise<string> => {
        return results.text().then((str): string => {
          const xml = new XMLParser().parseFromString(str)

          const pushContent = xml.getElementsByTagName('content')
          const contentSplit = pushContent[0].value.split(';')
          const finalArray = contentSplit[4].split('&')
          return finalArray[0].replace(',', '.')
        })
      }
    )

    const savingPercentOnMonth = (parseFloat(savingAPI) * 12) / 100

    setSavingPercent('saving', savingPercentOnMonth)
  }
}

import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
  html {
    font-size: 62.5%; /* 1 rem = 10px; 10px/16px = 62.5% */
    margin: 0;
    padding: 0;
    outline: 0;

    -webkit-user-select: none;
    -moz-user-select: -moz-none;
    -ms-user-select: none;
    user-select: none;
  }

  body {
    margin: 0;
    padding: 0;
    color: hsla(0, 0%, 0%, 0.8);
    word-wrap: break-word;
    /* background: linear-gradient(to right, #2a7998, #42a69d); */
  }

  a {
    background-color: transparent;
    -webkit-text-decoration-skip: objects;
  }
`
